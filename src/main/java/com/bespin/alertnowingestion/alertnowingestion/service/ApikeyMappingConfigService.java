package com.bespin.alertnowingestion.alertnowingestion.service;

import com.bespin.alertnowingestion.alertnowingestion.config.AlertConfig;
import com.bespin.alertnowingestion.alertnowingestion.modal.ApikeyMappingConfig;
import com.bespin.alertnowingestion.alertnowingestion.modal.MappingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ApikeyMappingConfigService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AlertConfig alertConfig;

    public ApikeyMappingConfig addTenantMapping(String apikey, String integrationType, String config){
        ApikeyMappingConfig existedApikeyMappingConfig = (ApikeyMappingConfig) redisTemplate.opsForHash().get(alertConfig.getRedisMap(), apikey);

        MappingConfig mappingConfig = new MappingConfig(integrationType, standardlizaedConfigString(config));
        if (Objects.isNull(existedApikeyMappingConfig)) {
            Map<String, MappingConfig> configs = new HashMap<>();
            configs.put(integrationType,mappingConfig);
            ApikeyMappingConfig apikeyMappingConfig = new ApikeyMappingConfig(apikey, configs);
            redisTemplate.opsForHash().put(alertConfig.getRedisMap(), apikey, apikeyMappingConfig);
            return apikeyMappingConfig;
        }else {
            Map<String, MappingConfig> configs = existedApikeyMappingConfig.getMappingConfigs();
            configs.put(integrationType,mappingConfig);
            existedApikeyMappingConfig.setMappingConfigs(configs);
            redisTemplate.opsForHash().put(alertConfig.getRedisMap(), apikey, existedApikeyMappingConfig);
            return existedApikeyMappingConfig;
        }
    }

    public MappingConfig getApiMappingConfig(String apikey, String integrationType){
        ApikeyMappingConfig existedApikeyMappingConfig = (ApikeyMappingConfig) redisTemplate.opsForHash().get(alertConfig.getRedisMap(), apikey);
        if (Objects.isNull(existedApikeyMappingConfig)) {
            existedApikeyMappingConfig = (ApikeyMappingConfig) redisTemplate.opsForHash().get(alertConfig.getRedisMap(), alertConfig.getDefaultKey());
        }
        return existedApikeyMappingConfig.getMappingConfigs().get(integrationType);
    }

    public MappingConfig deleteMappingConfig(String apikey, String integrationType){
        MappingConfig returnMapping = new MappingConfig();
        ApikeyMappingConfig existedApikeyMappingConfig = (ApikeyMappingConfig) redisTemplate.opsForHash().get(alertConfig.getRedisMap(), apikey);
        if (!Objects.isNull(existedApikeyMappingConfig)) {
            Map<String, MappingConfig> configs = existedApikeyMappingConfig.getMappingConfigs();
            returnMapping = configs.get(integrationType);
            if(!Objects.isNull(returnMapping)){
                configs.remove(integrationType);
            }
            existedApikeyMappingConfig.setMappingConfigs(configs);
            redisTemplate.opsForHash().put(alertConfig.getRedisMap(), apikey, existedApikeyMappingConfig);
        }
        return returnMapping;
    }

    private String standardlizaedConfigString(String config){
        config = config.replace("\t","");
        System.out.println(config);
        config = config.replace("\n","");
        System.out.println(config);
        config = config.replace("\"","'");
        System.out.println(config);
        return config;
    }
}
