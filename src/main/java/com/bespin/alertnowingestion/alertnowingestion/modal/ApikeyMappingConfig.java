package com.bespin.alertnowingestion.alertnowingestion.modal;

import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ApikeyMappingConfig extends JdkSerializationRedisSerializer implements Serializable  {
    private String apiKey;
    private Map<String, MappingConfig> mappingConfigs;
    private static final long serialVersionUID = -5227141083578548508L;

    public ApikeyMappingConfig(String apiKey, Map<String, MappingConfig> mappingConfigs) {
        this.apiKey = apiKey;
        this.mappingConfigs = mappingConfigs;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Map<String, MappingConfig> getMappingConfigs() {
        return mappingConfigs;
    }

    public void setMappingConfigs(Map<String, MappingConfig> mappingConfigs) {
        this.mappingConfigs = mappingConfigs;
    }
}
