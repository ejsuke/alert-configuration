package com.bespin.alertnowingestion.alertnowingestion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlertnowIngestionApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlertnowIngestionApplication.class, args);
	}

}
