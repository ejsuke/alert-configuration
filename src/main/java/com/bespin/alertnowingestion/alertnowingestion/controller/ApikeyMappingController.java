package com.bespin.alertnowingestion.alertnowingestion.controller;

import com.bespin.alertnowingestion.alertnowingestion.modal.ApikeyMappingConfig;
import com.bespin.alertnowingestion.alertnowingestion.modal.MappingConfig;
import com.bespin.alertnowingestion.alertnowingestion.service.ApikeyMappingConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/api")
public class ApikeyMappingController {
    @Autowired
    private ApikeyMappingConfigService apikeyMappingConfigService;


    @RequestMapping(value = "/new/{apikey}/{integration-type}", method = RequestMethod.POST)
    public ResponseEntity<ApikeyMappingConfig> addOrUpdateIntegrationType(@PathVariable(name = "apikey") String apikey, @PathVariable(name = "integration-type") String integrationType, @RequestBody String config){
        ApikeyMappingConfig apikeyMappingConfig = apikeyMappingConfigService.addTenantMapping(apikey,integrationType, config);
        return ok(apikeyMappingConfig);
    }

    @GetMapping(value = "/get/{apikey}/{integration-type}")
    public ResponseEntity<MappingConfig> getConfig(@PathVariable(name = "apikey") String apikey, @PathVariable(name = "integration-type") String integrationType){
        return ok(apikeyMappingConfigService.getApiMappingConfig(apikey,integrationType));
    }

    @DeleteMapping(value = "/delete/{apikey}/{integration-type}")
    public ResponseEntity<MappingConfig> deleteConfig(@PathVariable(name = "apikey") String apikey, @PathVariable(name = "integration-type") String integrationType){
        return ok(apikeyMappingConfigService.deleteMappingConfig(apikey,integrationType));
    }
}
